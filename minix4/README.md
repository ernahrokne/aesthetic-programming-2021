**MiniX4: _CAPTURE ALL_**

[_Code repository_](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix4/sketch.js)
and
[_my program_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix4/)
(remember to keep your sound on!)

This program also requires your webcamera to work!


**"Disguised surveillance"**

For this program I have created a little game where the user tries to catch the ball in their mouth and click the button when they manage it - but where I actually (try to) trick the user into taking a "funny" photo of themselves. This photo is combined with a sound from a meme, a gray filter and teardrops, hopefully making the user laugh a little from their dedication. For this week's miniX I had high ambitions - but my orginal idea proved to be too challenging, as well as not-so-great time managing. For days now I have been sitting with my screen divided in two - half atom and half p5 example with my face looking all the more desperate and ready to give up. I kinda got the feeling that maybe the user will in my program, and that I think is common when learning to code - that one is trying so hard to do something, and then when you finally do it, it looks superunimpressive and you feel kinda dumb! 

Even though it is a silly program, I also thought about the "darker" side to it. As I have been leaving the p5 example active the last couple of days, I can always see the little recording symbol on the tab - and it creeps me out. My program is not advanced, but with a few effects I can manage to manipulate an image of the user, and this is something that concerns me. As Shoshana Zuboff mentions in the documentary for this week's reading, Surveillance Capitalism, people tend to think that they have nothing to hide. While surveillance can come in many forms, I think that webcam surveillance is a concerning issue. Think about how often you allow your camera, in Zoom, Skype, on funny websites (don't worry - I don't have the skills to take over anyone's camera through my program), and the thought of someone accessing/hacking your camera is so frightening. As I see myself on the webcam in the p5 example, I too think to myself that I don't have anything to hide. And even if I were to do something I wouldn't want people to see, I close my computer or turn it away. But still, I flip the camera off once in a while, just to make sure that if there was somebody on the other end, they would get properly shocked...


**Describe your program and what you have used and learnt:**

In this weeks code I have learnt and used webcam-capture and filters, as well as `createButton`, `frameRate`, preloading and loading of image and sound, and `if` statements :) The program consists of the users webcam video x2 and a ball going up-down in a loop. If the user clicks the button everything freezes and some emojis and text appear! 

_answer to the next questions are coming_

![](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix4/screenshotminix4.png)
<img src="screenshotminix4.png" width="300">

_I finished up my project late on sunday, and remembered I needed a screenshot of it - hence the ready-to-sleep feel of it :)_
