_Disclaimer: The code for this sketch contains approx. 689 lines (: I tried to find out how to be able to open/load different sketches within the sketch, but I could not figure it out. However, I think the visual aspect ended up looking as I wanted!_

**Click [here](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix2/) to try it for yourself :)**

**Click [here](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix2/sketch.js) for the code repository.**

<img src="Screenshot_minix2.png" width="600">


**QUESTIONS FOR README:**

_1. Describe your program and what you have used and learnt_

My program consists of a big face constructed from different geometric 2D shapes, especially ellipses, round-edged rectangles, arcs and curves. I like to draw, and this face draws inspiration from the characters I usually tend to draw, and I wanted to try to construct it manually in p5. In addition I added a phone and some playful text to bring some interaction to my program. You decide between which emotion the character is feeling today; happy, sad, love or normal! When you decide on an emotion, a small message box appears with the fitting emojis, as if these emojis are supposed to accurately represent the emotion the character is feeling. I had a lot of fun constructing my program, even though it took a lot of time. My main "problem" was to shorten the syntax - I worked in four different sketches at the same time, and wanted to easily be able to "switch" between these in my main sketch, but it was a bit too complex for me at this point.


_2. How would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on?_

When I started my sketch, and from discussing the meaning of emojis with my group, I thought about how emojis are used as elements to represent our emotions in social media. As I really like to draw my characters with exaggerated wrinkles and lines, I thought about how emojis tend to lack exactly that. Therefore I kind of wanted to show the contrast between "real" emotion and the super-simplified emoji - and how those are supposed to be the same. Its not really a shot at showing emotions through emojis, just more about how it's interesting to think about how emojis play the part that they do in the 'modern' world (as referenced in my program). Emojis can contribute to quick, but also, lazy expression of emotions. This is a good addition to a busy and technological world, but sometimes it seems as they would replace actual conversation about feelings and emotions, where it does not seem "appropriate" to elaboratly write about them.
