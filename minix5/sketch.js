//All the variables are for the silver ball its movement :)
let y=0;
let x=0;
let goUp=false;
let goRight=false;
let ball;

function preload(){
  ball=loadImage("silverball.png");//the silver ball
}
function setup(){
  createCanvas(windowWidth,windowHeight);
}
function draw(){
  background(130,110,170,50);

//The streams of curves are put under for-loops for further control:
  for(let i=0; i<2; i++){//here I wanted 2 lines per frame (i<2)
  noFill();
  stroke(80,60,120,50);
  strokeWeight(40);
  curve(-100,-500,0,random(275,285),windowWidth,random(10,60),1000,500);//the purple stream
  stroke(250,100,150,50);
  strokeWeight(5);
  curve(-100,-1000,0,random(300,500),windowWidth/2,750,1000,500);//the pink stream
  }
  for(let i=0; i<500; i++){//here I wanted 500 lines per frame (i<500)
  noFill();
  stroke(random(0,130));
  strokeWeight(1);
  curve(-100,-500,0,300,windowWidth,random(50,windowHeight),1000,500);//the black & gray stream
  }

/* The ball and rings around the ball. When I put 'x' or 'y' as the elements' positions,
everything down below (the movements) counts for them: */
  noStroke();
  fill(200,70,80);
  ellipse(x+25,y+325,70,70)
  fill(180,150,20);
  ellipse(x+25,y+325,60,60);
  image(ball,x,y+300,50,50);

//Up & down movement:
  if(goUp==true){
  y=y-5;
  }
  if(goUp==false){
  y=y+5;
  }
  if(y>150){
  goUp=true;
  }
  if(y<20){
  goUp=false;
  }

//Right & left movement:
  if(goRight==true){
  x=x-7;
  }
  if(goRight==false){
  x=x+7;
  }
  if(x>windowWidth){
  goRight=true;
  }
  if(x<0){
  goRight=false;
  }
}
