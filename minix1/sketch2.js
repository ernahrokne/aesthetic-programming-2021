let mysong,mysound,img,img1,cloud9,regnbue,erna,signe,sofie,eva,thumbsup;

function preload() {
  mysong=loadSound("character.mp3");
  mysound=loadSound("yaaas.mp3");
  img=loadImage("cloud.png");
  img1=loadImage("balloon.png");
  cloud9=loadImage("9.png");
  regnbue=loadImage("regnbue.png");
  erna=loadImage("erna.png");
  signe=loadImage("signe.png");
  sofie=loadImage("sofie.png");
  eva=loadImage("eva.png");
  thumbsup=loadImage("thumbsup.png");
}
function setup() {
  createCanvas(2000,2000);
  background(20,250,200);
  let c=color(255,240,120);
  let c1=color(100,250,50);
  image(img,20,60,130,100);
  image(img,560,40,110,90);
  image(img,270,300,150,110);
  image(img1,200,280,100,150);
  image(img1,630,100,100,150);
  image(img1,120,10,80,120);

  buttonstart=createButton("CLICK HERE TO START:) (sound on!)");
  buttonstart.style('background-color',c1);
  buttonstart.position(273,140);
  buttonstart.size(180,90);
  buttonstart.mousePressed(start);

  button=createButton("CLOUD9 (studygroup 9)");
  button.style('background-color',c);
  button.position(80,2000);
  button.size(250,110);
  button.mousePressed(changeBG);
  button.mouseOver(make);

  button1=createButton("another studygroup..");
  button1.style('background-color',c);
  button1.position(400,2000);
  button1.size(250,110);
  button1.mouseOver(makeNew);
}
function start() {
  start=mysong.play();
  textSize(20);
  fill(0,0,0,100);
  text('*CHOOSE',195,127);
  fill(255);
  text('*CHOOSE',190,120);
  fill(0,0,0,100);
  text('YOUR',320,127);
  fill(255);
  text('YOUR',315,120);
  fill(0,0,0,100);
  text('CHARACTER*',415,127);
  fill(255);
  text('CHARACTER*',410,120);
  textSize(13);
  fill(255);
  text('*studygroup*',467,95);
  buttonstart.remove();
  button1.position(400,170);
  button.position(80,170);
  fill(0,0,0,70);
  noStroke();
  rect(88,178,250,110);
  rect(408,178,250,110);
}
function changeBG() {
  changeBG=background(random(100,255),random(100,255),random(100,255));
  textSize(20);
  fill(random(0,200),random(0,200),random(0,200));
  text('yay',320,430);
  text('cool',30,120);
  textSize(15);
  text('superb!',100,500);
  text('nice.',550,40);
  text('wow',590,270);
  text('woo!',390,240);
  text('champ',590,160);
  text('nice',530,470);
  text('good choice hehe',215,160);
  fill(255);
  textSize(15);
  text('*signe approves*',440,260);
  text('*sofie approves*',180,430);
  text('*eva approves*',500,380);
  text('*erna approves*',320,40);

  image(img,110,30,110,80);
  image(img,460,40,150,110);
  image(img,580,290,110,80);
  image(img,300,340,90,70);
  image(img,20,285,140,100);
  image(cloud9,70,100,70,70);
  image(cloud9,450,270,50,50);
  image(regnbue,320,230,120,110);
  image(thumbsup,270,285,55,60);
  image(thumbsup,570,390,65,70);
  image(thumbsup,210,50,65,70);
  image(erna,320,40,100,120);
  image(signe,470,150,100,100);
  image(sofie,170,300,100,120);
  image(eva,440,320,80,120);
  mysound.play();
  mysong.stop();
  button1.remove();
  rect.remove();
}
function make() {
  make=createButton("yees, don't be shy! click to change the colorrrss")
  make.position(180,180);
}
function makeNew() {
  makeNew=createButton("uhm!!! don't even try clicking");
  makeNew.position(540,180);
  button1.mousePressed(makeNew0);
  make.remove();
}
function makeNew0() {
  makeNew0=createButton("  :(  ");
  makeNew0.position(105,160);
  makeNew.remove();
  button1.mousePressed(makeNew1);
}
function makeNew1() {
  makeNew1=createButton("wow, again?");
  makeNew1.position(370,200);
  makeNew0.remove();
  makeNew.remove();
  button1.mousePressed(makeNew2);
}
function makeNew2() {
  makeNew2=createButton("really?! just choose Cloud9 already..");
  makeNew2.position(435,215);
  makeNew1.remove();
  button1.mousePressed(done);
}
function done() {
  makeNew2.remove();
  makeNew1.remove();
  makeNew0.remove();
  makeNew.remove();
  button1.remove();
  fill(20,250,200);
  noStroke();
  rect(408,178,250,110);
}
function draw() {
  fill(255);
  stroke(0);
  ellipse(20,20)
}
