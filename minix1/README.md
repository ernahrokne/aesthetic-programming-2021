[**Link to my project**](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix1/)
and
[**link to the code**](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix1/sketch2.js)
:)

(Remember to keep your sound on!)

![](abc.jpg)
<img src="Screenshot-sketch2.png" width="450" height="250">
![](abc.jpg)
<img src="Screenshot-sketch2-2.png" width="390" height="250">

**Questions to think about in your ReadMe:**
1. I have made a little "questionnaire" where the options has different outcomes if selected. It gives you a choice between our studygroup (cloud9) and another studygroup, and uses sounds, images and buttons popping up, to make it fun and playful :) It is not meant to offend anybody btw, its just for jokes! I knew that I wanted to use a "choose your character" sound, but I realized that Google does not like when websites uses autoplay - which I found interesting, and I'll definitely remember this in the future. I didn't plan the idea beforehand, just played around with different p5.js references, and ended up with this. Mainly I have focused on the button('createButton') reference, and sound and images with the 'preload', 'loadSound' and 'loadImage' references. I have made it similar to a game, with a start and end, where the different actions leads to new options, and used references like 'example.remove', 'example.start' and 'example.stop'.

2. I think my first coding experience with p5.js has been really fun and exciting. It was very fun to explore the references, and I realized the more I worked with my sketch, the more inspiration I got for other things I wanted to include. I also think it was fairly easy to understand the codes I've used so far, by watching the Coding Train on youtube and searching around the p5.js website. It also helped me to easier identify my mistakes!

3. When coding I think once you understand the words and codes you're typing, it's not so different from normal writing. For me, the words and functions makes a lot of sense, and it's rather the different signs, like ';' and '}', that are harder to understand when to type and not type. The language in p5.js also seems very userfriendly, and I think its nice that they use whole, logical words (fx. loadSound, instead of ldSnd), but also gives the user options to create their own names and functions!

4. Coding and programming has always sounded super-complex, but as I've started to learn from a very basic-beginner level, I think it makes a lot of sense. I think especially when watching Daniel Shiffman and Lauren Mccarthy, they make coding feel more accessible and inclusive. It is also very fun, and especially when seeing your own produced code in action - the contrast between the long and boring/confusing-looking numbers and letters and the visually exciting image is really a motivator!****
