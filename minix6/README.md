# MiniX6

[RunMe](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix6/)
[Code repository](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/blob/master/minix6/sketch.js)

Minix6 on the left and minix3 on the right:)

<img src="minix6.png" height="250"> vs. <img src="Screenshot_minix3.png" height="250">

**Which MiniX do you plan to rework?**
Hello! This week I decided to revisit my minix3 - the throbber. This was based on the feedback I received for that minix (from Anders and Jakob - supernice and constructive, thank you so much!), specifically this little quote: "When we look at the program we get connotations towards marriage. It seems that the finger is having troubles putting on the ring, which could symbolize the struggles that many people in the modern world face when it comes to love and obligation. We think this has a nice symbolic value to it."

When I made my minix3 - I did actually think about the connotations one might get towards marriage, but I didn't really mention it. Therefore, I thought that was something I wanted to work further on.

**What have you changed and why?**
For this program I have not done too much. Some small aesthetic changes (like different background color etc.), but mainly I have added a finger. This finger is supposed to look old and wrinkly, and whenever it touches the ring, it slows down the speed of the fingers. Neither one of the fingers, the young and the old one, have any ring. I want my program to give an impression of a young person trying eagerly to put the ring on, while the old person is trying to speed the ring up to signalize that "hey, there is no rush...". What does marriage mean to a person today? There is absolutely pressure on young people to get married, but the idea of marriage is heavily influenced by tradition. As around half of marriages today end up in divorce, why do we chase after an idea of everlasting love, when there is no certainty that it exists? I think that my program is up for interpretation, and I don't intend for  it to be biased, or with any clear "side".

**How would you demonstrate aesthetic programming in your work?**
In my work I can use my creativity to make something artistic and visual to express a certain issue. As we get subjects each week to reflect upon and work with, I can work with programming to express my thoughts and opinions on the matter, or even just as a reflection.
**What does it mean by programming as a practice, or even as a method for design?**
I think programming is a really exciting and interesting way of expressing oneself - especially through art and design. Programming is always evolving, and gives one the opportunity to re-iterate and revisit. It is also interesting to work with subjects that may not be related to technology, and seeing how it can be transfered into programming.
**What is the relation between programming and digital culture?**
**Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?**


