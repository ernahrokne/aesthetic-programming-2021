let picture;
let y=30;
let goUp=false;//the movement

function preload(){
  picture=loadImage("diamond.png");
}
function setup(){
  createCanvas(windowWidth,windowHeight);
}
function draw(){
  background(100);
  drawElements();
}
function drawElements(){
push();
  translate(width/2,350);
  rotate(millis()/275);
//the diamond
  image(picture,-55,50,90,90);
//the ring
  noStroke();
  fill(0);
  ellipse(0,0,157,157);//outer black line
  fill(255);
  ellipse(0,0,150,150);//the ring highlight
  fill(200,200,20);
  ellipse(0,0,140,150);//the yellow color
  fill(0,0,0,130);
  ellipse(0,0,133,130)//the ring shadow
  fill(0);
  ellipse(0,0,121,121);//the inner black ring
  fill(100);
  ellipse(0,0,115,115);//the grey in the middle to shape the ring
pop();
  translate(width/2,height/2);
  noStroke();
//the finger
  fill(0);
  ellipse(0,y-450,66,231);//black lines
  ellipse(0,y-350,66,181);//black lines
  ellipse(0,y-260,56,141);//black lines
  fill(220,150,20);
  ellipse(0,y-450,60,225);//finger
  ellipse(0,y-350,60,175);//finger
  ellipse(0,y-260,50,135);//finger
  fill(0);
  ellipse(23,y-225,18,116);//fingernail
  fill(220,150,150);
  ellipse(23,y-225,12,110);//fingernail
  fill(0,0,0,60);
  ellipse(-20,y-440,20,77);//finger shadow
  ellipse(-20,y-350,20,90);//finger shadow
  ellipse(-18,y-250,15,70);//finger shadow
  fill(255,255,255,100);
  ellipse(22,y-200,5,40);//fingernail highlight
  ellipse(-5,y-210,15,25);
  noFill();
  stroke(0);
  arc(20,y-430,20,10,PI,TWO_PI);//finger lines
  arc(20,y-420,20,3,PI,TWO_PI);//finger lines
  arc(18,y-415,25,10,0,PI,TWO_PI);//finger lines
  arc(20,y-350,25,5,PI,TWO_PI);//finger lines
  arc(20,y-340,20,5,0,PI,TWO_PI);//finger lines

//bottom finger
  noStroke();
  fill(0);
  ellipse(0,y+350,61,231);//black lines
  ellipse(0,y+280,56,181);//black lines
  ellipse(0,y+180,51,176);//black lines
  ellipse(10,y+250,46,26);//finger bumps
  ellipse(12,y+260,46,26);//finger bumps
  ellipse(10,y+270,46,31);//finger bumps
  ellipse(10,y+310,46,31);//finger bumps
  ellipse(10,y+320,46,36);//finger bumps
  fill(200,170,70);
  ellipse(0,y+350,55,225);//finger
  ellipse(0,y+280,50,175);//finger
  ellipse(0,y+180,45,170);//finger
  ellipse(10,y+250,40,20);//finger bumps
  ellipse(12,y+260,40,20);//finger bumps
  ellipse(10,y+270,40,25);//finger bumps
  ellipse(10,y+310,40,25);//finger bumps
  ellipse(10,y+320,40,30);//finger bumps
  fill(0,0,0,60);
  ellipse(-15,y+180,20,70);//finger shadow
  fill(0);
  ellipse(20,y+155,20,111);//fingernail
  fill(150,120,50);
  ellipse(20,y+155,14,105);//fingernail
  fill(250,230,255,50);
  ellipse(15,y+270,20,10);//highlight
  ellipse(20,y+255,20,10);//highlight
  ellipse(+20,y+160,10,30);//highlight

  noFill();
  stroke(0);
  arc(15,y+250,20,3,PI,TWO_PI);//finger lines
  arc(15,y+265,25,5,PI,TWO_PI);//finger lines
  arc(16,y+275,25,10,0,PI,TWO_PI);//finger lines
  arc(15,y+315,20,10,PI,TWO_PI);//finger lines
  arc(15,y+320,20,5,0,PI,TWO_PI);//finger lines
  if(goUp==false){
  y=y+2;
  }
  if(goUp==true){
  y=y-2;
  }
  if(y>90){
  goUp=true;
  frameRate(80);
  }
  if(y<-10){
  goUp=false;
  frameRate(20);
  }
}
