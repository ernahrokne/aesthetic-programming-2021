let picture;
let y=30;
let goUp=false;//the movement

function preload(){
//the diamond in the sketch is an image
  picture=loadImage("diamond.png");
}
function setup(){
  createCanvas(windowWidth,windowHeight);
}
function draw(){
  background(50);
  drawElements();
}
function drawElements(){
push();
  translate(width/2,500);
  rotate(millis()/275);
//the diamond
  image(picture,-55,50,120,120);
//the ring (only yellow part)
  noStroke();
  fill(0);
  ellipse(0,0,187,187);//outer black line
  fill(255);
  ellipse(0,0,180,180);//the ring highlight
  fill(200,200,20);
  ellipse(0,0,170,180);//the yellow color
  fill(0,0,0,130);
  ellipse(0,0,153,160)//the ring shadow
  fill(0);
  ellipse(0,0,146,146);//the inner black ring
  fill(50);
  ellipse(0,0,140,140);//the grey in the middle to shape the ring
pop();
  translate(width/2,0);
  noStroke();
//the finger
  fill(0);
  ellipse(0,y-15,97,257);//black lines
  ellipse(0,y+100,97,207);//black lines
  ellipse(0,y+200,87,177);//black lines
  fill(220,150,20);
  ellipse(0,y-15,90,250);//finger
  ellipse(0,y+100,90,200);//finger
  ellipse(0,y+200,80,170);//finger
  fill(0);
  ellipse(35,y+250,25,147);//fingernail
  fill(220,150,150);
  ellipse(35,y+250,18,140);//fingernail
  fill(0,0,0,60);
  ellipse(-32,y+200,20,77);//finger shadow
  ellipse(-38,y+95,20,80);//finger shadow
  ellipse(-38,y-20,25,120);
  fill(255,255,255,100);
  ellipse(35,y+280,10,60);//fingernail highlight
  ellipse(-15,y+250,20,40);
  noFill();
  stroke(0);
  arc(25,y+80,40,10,PI,TWO_PI);//finger lines
  arc(30,y+95,30,5,PI,TWO_PI);//finger lines
  arc(25,y+110,40,10,0,PI,TWO_PI);//finger lines
  arc(30,y-10,30,5,PI,TWO_PI);//finger lines
  arc(25,y,40,5,0,PI,TWO_PI);//finger lines
  if(goUp==false){
  y=y+2;
  }
  if(goUp==true){
  y=y-2;
  }
  if(y>90){
  goUp=true;
//the little spark when the nail and ring touches
  fill(0);
  noStroke();
  triangle(40,395,15,410,40,425)
  triangle(40,395,65,410,39,425)
  triangle(25,410,40,380,55,410)
  triangle(25,410,40,440,55,410)
  fill(255);
  triangle(40,400,20,410,40,420)
  triangle(40,400,60,410,39,420)
  triangle(30,410,40,385,50,410)
  triangle(30,410,40,435,50,410)
  }
  if(y<-10){
  goUp=false;
  }
}
