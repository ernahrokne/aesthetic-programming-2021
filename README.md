### Hello & welcome😇🌸💖

This is my aesthetic programming folder where you can find all my projects in the AP course of 2021. Enjoy exploring! Down below are links (& some gifs) to all the programs' RunMe, for super-easy access 😉

----

[Minix1 - My first Program: _"CHOOSE YOUR STUDY GROUP"_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix1/)

<img src="../libraries/minix's/xgifs/minix1.gif" width="300">

[Minix2 - Geometric Emoji](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix2/)

<img src="../libraries/minix's/xgifs/minix2.gif" width="300">

[Minix3 - Designing a Throbber](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix3/)

<img src="../libraries/minix's/xgifs/minix3.gif" width="300">

[Minix4 - Capture all](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix4/)

<img src="../libraries/minix's/xgifs/minix4.gif" width="300">

[Minix5 - A generative program: _"An Organized Stream of Chaos"_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix5/)

<img src="../libraries/minix's/xgifs/minix5.gif" width="300">

[Minix6 - Revisit/rework on Minix3](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix6/)

<img src="../libraries/minix's/xgifs/minix6.gif" width="300">

[Minix7 - Games with Objects: _"The Social Distance Game"_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix7/)

<img src="../libraries/minix's/xgifs/minix7.gif" width="300">

[Minix8 - E-Lit: _"Corona Guidelines That Will Absolutely, 100% Work"_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix8/)

_In collaboration with [Signe Regin Runge-Dalager](https://gitlab.com/Signe_RD/aesthetic-programming)_

<img src="../libraries/minix's/xgifs/minix8.gif" width="300">

[Minix9 - Que(e)ry data: _"The Giphyfier"_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix9/)

_In Collaboration with [Sofie Juul Nisted](https://gitlab.com/sofienisted/aesthetic-programming)_

<img src="../libraries/minix's/xgifs/minix9.gif" width="300">

[Minix10 - Flowchart](https://gitlab.com/ernahrokne/aesthetic-programming-2021/-/tree/master/minix10%20-%20flowcharts)

---

Hope you enjoy!
