### All mini-exercises😇🌸💖

[_Minix1 - My first Program_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix1/)

<img src="../minix's/xgifs/minix1.gif" width="300">

[_Minix2 - Emoji_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix2/)

<img src="../minix's/xgifs/minix2.gif" width="300">

[_Minix3 - A throbber_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix3/)

<img src="../minix's/xgifs/minix3.gif" width="300">

[_Minix4 - Capture all_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix4/)

<img src="../minix's/xgifs/minix4.gif" width="300">

[_Minix5 - A generative program_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix5/)

<img src="../minix's/xgifs/minix5.gif" width="300">

[_Minix6 - Revisit/rework on Minix3_](https://ernahrokne.gitlab.io/aesthetic-programming-2021/minix6/)

<img src="../minix's/xgifs/minix6.gif" width="300">
